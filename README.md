# Featherweight Queue Trasnport - Beanstalkd<br />
Cal Evans <cal@calevans.com>

This is the beanstalkd transport for Featherweight Queue. I do not recommend using this transport in production. It is mainly for development.

# Configuration

Add these entries to your `.env` file.

```
BEANSTALKD_SERVER=''
BEANSTALKD_PORT=''
BEANSTALKD_CONNECTION_TIMEOUT=''
```