<?php

declare(strict_types=1);

namespace Eicc\Fwq\Transport\Beanstalkd;

use Eicc\Fwq\Exceptions\InvalidQueueException;
use Eicc\Fwq\Exceptions\QueueEmptyException;
use Eicc\Fwq\Models\AbstractTransport;
use Pimple\Container;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * This is the Beanstalkd Trasnport.
 *
 * Beanstalkd uses the term "tube" to mean a queue. For consistency's sake, we
 * stick with the name queue. If you are familair with beanstalkd, just do the
 * translation in your head.
 *
 * Queues are ephemeral in beanstalkd. They are created as soon as you write a
 * job to it and they go away when there are no more jobs in it.
 *
 * https://github.com/beanstalkd/beanstalkd/blob/master/doc/protocol.txt
 *
 * Exceptions needed:
 *       ServerNotConnectedException
 *       ConnectionTimeoutException
 *       CreateJobException
 *       FailedToSelectQueueException
 *       FailedToReserveJob
 */
class Transport extends AbstractTransport
{
  const LINE_ENDING = "\r\n";
  const PRIORITY = 100;
  const TIME_TO_RUN = 600;

  protected ?Container $container;
  protected $connection;
  protected ?string $server;
  protected int $port = 11300;
  protected int $timeout = 10;
  protected bool $connected = false;

  public function __construct(Container $container)
  {
    $this->container = $container;
    $this->server = $_ENV['BEANSTALKD_SERVER'];
    $this->port = (int)$_ENV['BEANSTALKD_PORT'];
    $this->timeout = (int)$_ENV['BEANSTALKD_CONNECTION_TIMEOUT'];
  }

  public function __destruct()
  {
    $this->disconnect();
  }

  public function push(object $luw, \DateTimeImmutable $runAfter, string $queueName): void
  {
    $data = json_encode($luw);

    $delay = (int)$runAfter->format('U') - time();
    $delay = ($delay < 0) ? 0 : $delay;

    $this->connect();

    $this->setWriteQueue($queueName);

    $this->write(
        sprintf(
            "put %d %d %d %d%s%s",
            self::PRIORITY,
            $delay,
            self::TIME_TO_RUN,
            strlen($data),
            self::LINE_ENDING,
            $data
        )
    );

    $status = strtok($this->read(), ' ');

    switch ($status) {
      case 'INSERTED':
        // we are good. NO OP
          break;
      case 'EXPECTED_CRLF':
      case 'JOB_TOO_BIG':
      case 'BURIED':
        $this->container['log']->error('There was an error writing a job to the server. Status : ' . $status);
          throw new \Exception('There was an error writing a job to the server. Status : ' . $status);
      break;
      case 'DRAINING':
        $this->container['log']->error('Server is in DRAINING mode. Job not accepted.');
          throw new \Exception('Server is in DRAINING mode. Job not accepted.');
      break;
      default:
        $this->container['log']->error('There was an error writing a job to the server. Status: ' . $status);
          throw new \Exception('There was an error writing a job to the server. Status: ' . $status);
    }
  }

  public function pop(string $queueName, string $workerName): object
  {

    $this->connect();
    $this->setReadQueue($queueName);
    $job = $this->reserveJob();

    if (is_null($job)) {
      throw new QueueEmptyException($queueName . ' is empty or does not have any jobs ready to run.');
    }

    return $job;
  }

  public function jobCount(string $queueName): int
  {
    $this->connect();
    $stats = $this->readStats();
    return (int) ($yaml['current-jobs-ready'] + $yaml['current-jobs-urgent'] + $yaml['current-jobs-delayed']);
  }

  public function jobList(string $queueName): array
  {
    throw new \Exception('Not supported for this server type.');
  }

  public function getSetting(string $queueName, string $key)
  {
    throw new \Exception('Not supported for this server type.');
    // NO OP for beanstalkd.
  }

  public function setSetting(string $queueName, string $key, $value): void
  {
    throw new \Exception('Not supported for this server type.');
    // NO OP for beanstalkd.
  }

  public function createQueue(string $queueName, array $parameters): void
  {
    // NO OP for beanstalkd.
  }

  public function destroyQueue(string $queueName): void
  {
    // NO OP for beanstalkd.
  }

  public function doesQueueExist(string $queueName): bool
  {
    return true;
  }

  public function listQueues(): array
  {
    $this->connect();
    $this->write('list-tubes');
    $rawMessage = $this->read();
    $status = strtok($rawMessage, ' ');

    if ($status !== 'OK') {
      $this->container['log']->error('Error reading the list of queues from the server.', [$status]);
      throw new \Exception('Error reading the list of queues from the server. Status :' . $status);
    }

    return $this->processYaml($this->read((int)strtok(' ')));
  }

  public function getMeta(string $queueName): object
  {
    throw new \Exception('Not supported for this server type.');
  }

  public function initalize(): void
  {
    throw new \Exception('Not supported for this server type.');
    // NO OP for beanstalk
  }


  /*
   * METHODS NOT PART OF THE INTERFACE
   */
  protected function connect(): void
  {
    if (isset($this->connection)) {
      $this->disconnect();
    }

    $this->connection = fsockopen(
        $this->server,
        $this->port,
        $errorNumber,
        $errorMessage,
        $this->timeout
    );

    if (!empty($errNum) || !empty($errStr)) {
      $this->container['log']->error('Error connecting to ' . $this->server . ' on port ' . $this->port);
      throw new \Exception('Error connecting to ' . $this->server . ' on port ' . $this->port);
    }


    if (is_resource($this->connection)) {
      stream_set_timeout($this->connection, -1);
    }
  }

  protected function disconnect()
  {
    $this->container['log']->debug('Disconnecting from server.');
    if (is_resource($this->connection)) {
      $this->write('quit');
      fclose($this->connection);
      $this->connection = null;
    }
  }

  /**
   *
   * @todo custom exceptions here.
   *       ServerNotConnectedException
   *       ConnectionTimeoutException
   */
  protected function read(int $bytes = null): string
  {
    $returnValue = '';

    if (!feof($this->connection)) {
      if (is_null($bytes)) {
        $returnValue = stream_get_line($this->connection, 16384, "\r\n");
      } else {
        $returnValue = stream_get_contents($this->connection, $bytes + 2);
      }

      $meta = stream_get_meta_data($this->connection);

      if ($meta['timed_out']) {
        $this->container['log']->error('Connection timed out while reading data from socket.');
        throw new \Exception('Connection timed out while reading data from socket.');
      }
    }

    $returnValue = rtrim($returnValue, self::LINE_ENDING);
    $this->container['log']->debug('Read Data', [$returnValue]);
    return $returnValue;
  }

  protected function write(string $data): void
  {
    $data .= self::LINE_ENDING;
    $this->container['log']->debug('Write Data', [$data]);
    fwrite($this->connection, $data);
  }

  // Not in use but required for the interface.
  public function setQueue(string $queueName): void
  {
  }

  /*
   * READ needs to send the WATCH command.
   */
  protected function setReadQueue(string $queueName): void
  {
    $this->container['log']->debug('Setting queue to ' . $queueName);

    $this->write(sprintf('watch %s', $queueName)); // read
    $status = strtok($this->read(), ' ');

    if ($status !== 'WATCHING') {
      $this->container['log']->error('There was an error setting queue to ' . $queueName, [ $status ]);
      throw new \Exception('There was an error setting queue to ' . $queueName . ' Status Returned :' . $status . '.');
    }


    foreach ($this->listQueuesBeingWatched() as $watchedQueue) {
      if (strtolower(trim($watchedQueue)) === strtolower(trim($queueName))) {
        continue;
      }
      $this->ignoreQueue($watchedQueue);
    }
  }

  /*
   * WRITE needs to send the USE command.
   */
  public function setWriteQueue(string $queueName): void
  {
    $this->container['log']->debug('Setting write queue to ' . $queueName);

    $this->write(sprintf('use %s', $queueName));
    $status = strtok($this->read(), ' ');

    if ($status !== 'USING') {
      $this->container['log']->error('There was an error setting queue to ' . $queueName, [ $status ]);
      throw new \Exception('There was an error setting queue to ' . $queueName . ' Status Returned :' . $status . '.');
    }
  }


  protected function deleteJob(int $id): void
  {
    $this->write(sprintf('delete %d', $id));
    $status = $this->read();

    switch ($status) {
      case 'DELETED':
          break;
      case 'NOT_FOUND':
      default:
        $this->container['log']->error('There was an error setting queue to ' . $queueName . ' Status Returned :' . $status . '.');
          throw new \Exception('There was an error setting queue to ' . $queueName . ' Status Returned :' . $status . '.');
    }
  }

  protected function reserveJob(): object|null
  {
    // Workers don't wait on jobs. They either exist or the next worker can deal with it.
    $this->write('reserve-with-timeout 2');
    $status = strtok($this->read(), ' ');
    $luw = null;

    switch ($status) {
      case 'RESERVED':
        $jobId = (int) strtok(' ');
        $rawJob = $this->read((int) strtok(' '));
        $job = json_decode($rawJob);
        $this->deleteJob($jobId);
        $job->jobId = $jobId;
          return $job;
      break;

      case 'TIMED_OUT':
      case 'DEADLINE_SOON':
        // NO OP. We don't wait for jobs so this is normal
          break;

      default:
        $this->container['log']->error('There was an error reserving a job.', [$status]);
          throw new \Exception('There was an error reserving a job.');
    }

    return null;
  }

  /**
   * @todo move Yaml to container.
   */
  protected function processYaml(string $rawYaml): array
  {
    $this->container['log']->debug('processYaml Raw Yaml', [$rawYaml]);

    try {
      return Yaml::parse($rawYaml);
    } catch (ParseException $exception) {
      $this->container['log']->error('Unable to parse the YAML string: %s', [$exception->getMessage(),$rawYaml]);
      throw new \Exception('Error parsing YAML while reading the list of queues.');
    }

    return[];
  }

  // Untested
  protected function readStats(): array
  {
    $this->write('stats');
    $rawMessage = $this->read();
    $status = strtok($rawMessage, ' ');

    if ($status !== 'OK') {
      $this->container['log']->error('Error reading stats from the server.', [$status]);
      throw new \Exception('Error reading stats from the server. Status :' . $status);
    }

    $bytes = (int)strtok(' ');

    return $this->processYaml(trim(strtok(' '), self::LINE_ENDING));
  }

  protected function listQueuesBeingWatched()
  {
    $this->write('list-tubes-watched');
    $rawMessage = $this->read();
    $status = strtok($rawMessage, ' ');

    if ($status !== 'OK') {
      $this->container['log']->error('Error reading the list of queues from the server.', [$status]);
      throw new \Exception('Error reading the list of queues from the server. Status :' . $status);
    }

    return $this->processYaml($this->read((int)strtok(' ')));
  }

  protected function ignoreQueue(string $queueName): void
  {
    $this->write(sprintf('ignore %s', $queueName));
    $status = strtok($this->read(), ' ');

    if ($status !== 'WATCHING') {
      $this->container['log']->error('Error ignoring a queue.', [$status]);
      throw new \Exception('Error ignoring a queue. Status :' . $status);
    }

    $this->container['log']->error('After ignoring a queue, we are still watching this many.', [strtok(' ')]);
  }
}
